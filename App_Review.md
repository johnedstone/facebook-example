# Steps to get a simple App to pass review

## First
* Create a page in your account, on facebook.com.
You will use this later for your app.

## Create an App
* Create a new App at developers.facebook.com under
the link MyApp choosing type=None

![create_app.png](images/create_app.png)

* Click through to details

![app_details.png](images/app_details.png)

* Under Basic settings add
    * App icon

    * Email

    * URL to private policy which you can generate and post on your website,
    e.g. [an example of generatng a free private policy](https://www.freeprivacypolicy.com/free-privacy-policy-generator/)

    * Note: for server to server app, you website only needs two pages,
    the index.html and the private policy
 
![basic_settings.png](images/basic_settings.png)

* Continuing down in Basic settings add

    * Further down is Individual and Business Verifiation.
    Follow the steps to get Individual Review.  Business
    Verification will not be dealt with here

    * And lastly, scroll down and add a Platform, selecting "Website"

![add_platform.png](images/add_platform.png)

## Review settings
* After the App is developed, then the Review settings can begin.

* Two good resources, as noted elsewhere are
[FB documentation: Create an App](https://developers.facebook.com/docs/development/create-an-app/server-to-server-apps) and
[FB documentation: Sample Submission](https://developers.facebook.com/docs/app-review/resources/sample-submissions/server-to-server/)

* The current App required the permission *pages_manage_posts* and two
dependencies which are not being used.

### Permissions: pages_manage_posts
* In the following screenshot it was noted that FB could not review
my app because it was posting messages from another server ("my website")

* And in the permission description for pages_manage_posts
it was explicitly noted that the endpoint */page/feed* will be used to POST

* And lastly, note that the video provided below was a screenshot (movie).  It was known that
based on the first failed review, FB was looking for the words *Just now* as the
message was posted. So, this movie explicitly showed this (see below)

* Regarding what endpoints each permissions use, this can be found at
this link [for example, pages_manage_posts permission](https://developers.facebook.com/docs/permissions/reference/pages_manage_posts)

![pages_manage_post_App_Review.png](images/pages_manage_post_App_Review.png)

* **And the video submitted for review from FB is below - click in the lower left for fullscreen**

![FB_App_Review.mp4](images/FB_App_Review.mp4)


### Permissions: the two (unused) dependency permissions

* In the following screenshot, regarding the two dependent permissions
that were not being used, it was important to state that I would not be using the
endpoints and/or the functionality:

    * In one it was explicitly stated that the endpoint would not be used

    * And in the other, it was explicitly stated that none of the functionality described in
    the permission would be used.

    * In both I referenced the first video, since a video is a requirement

![two_dependents.png](images/two_dependents.png)
