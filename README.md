# facebook-example
___As of 31-Aug-2021 the facebook page for these examples is this [https://www.facebook.com/people/Python-fb-api/100071653786926/](https://www.facebook.com/people/Python-fb-api/100071653786926/)___ or [https://facebook.com/johnedstone.python.fb.api](https://facebook.com/johnedstone.python.fb.api)

## Server to Server App Review
References:
* https://developers.facebook.com/docs/app-review/resources/sample-submissions/server-to-server/
* https://developers.facebook.com/docs/development/create-an-app/server-to-server-apps 

### Business Verification reference
* https://developers.facebook.com/docs/development/release/business-verification 

## To do: Unsolvable
* Feeds created with these scripts do not show up on the public feed,
  though `is_published: true`. My conclusion is as follows
  ```
  The api works, no problem.  But in order to post live using an app,
  currently you have to go through a Business Verification.  That is,
  no documentation has been found, currently (on Google or in FB)
  that describes using the app, as an individual, to post public to a page.
  The app posts, but it is not public, and can only be copied on the back end,
  to make it public : (.
  One can turn on live without Business verification, and the past app posts
  will become public, but then no posting by the app is possible, in this live mode
  as the permissions are then "not right"
  ```
    
    * __Actually__: *Sharing* as the user may be a work around - no, this did not help
    * __Workaround__: Go into the business.facebook.com and duplicate post, and delete original post : (
  

## Getting started with access tokens
* Use Graph API Explorer to get a short-lived User Access token (select page)
* Check with token debugger, it will be a 1 hour User token
* Use `python access_tokens/get_long_lived_token.py` to get long-lived
  User Access token using the short-lived User Access Token. This script uses
  the python request library.  Of course, curl will work as well.
  One can also click the button on the token debugger page to get this
  long-lived User Access token.
* Check with token debugger, it will be a 2 month User token.
* Use `python access_tokens/get_permanent_page_token.py` to get a long-lived
   Page Access token (which never expires) using the long-lived User Access
   Token. This script uses the python request library.  Again, curl would work.
   __Note: in order to post a feed the following permissions are needed:__ 
   `pages_show_list, pages_read_engagement, pages_manage_posts, public_profile`
* Check with token debugger, this will be a Page token that never expires.
   Save this token.  A new never-expiring Page token is generated whenever
   this endpoint is requested. The previous never-expiring Page token is
   still valid.

## Examples for pages
* `python page_requests/test_api_request.py`: uses python library to test such things
    as `/me/?metadata=` which returns all of the metadata depending on the
    token.
* `page_requests/put_feed.py`: posts a message in a feed
* More examples can be found by reading this help in the python shell
    ```
    >>> import facebook
    >>> help(facebook)
    ```
and by reading [python facebook docs](https://facebook-sdk.readthedocs.io/en/latest/api.html)
* See the following documention for creating content, i.e. posting a feed on a page:
    * [Start with Page](https://developers.facebook.com/docs/graph-api/reference/page/)
    * and see the Edges section and click on [Feed](https://developers.facebook.com/docs/graph-api/reference/v11.0/page/feed)
    * and on the Feed page is the description of creating content, i.e posting a feed
    * Here is a ___screenshot of that section___ showing the Post and the permissions needed ![screenshot](images/create_content_task.png)

## Learning points
* 2021-09-16: [One can not create a post on one's profile page - looks like it has to be "page"](https://developers.facebook.com/docs/graph-api/reference/user/feed)
* 2021-09-21: select token for page first
* 2021-09-21: Using request, v11.0, but with the python facebook-sdk, the latest is 3.1
* 2021-09-22: `/me?metadate=1` does not appear to be documented in the API guide - this gets metadata depending on the token (e.g User token or Page token)

Python example for Facebook API

* [python sdk library](https://facebook-sdk.readthedocs.io/en/latest/api.html)
* [python sdk library github](https://github.com/mobolic/facebook-sdk)
* [Reference API Post](https://developers.facebook.com/docs/reference/api/post/)

## Access Tokins: getting the permanent page token
### References
* [Facebook tokens documentation](https://developers.facebook.com/docs/facebook-login/access-tokens/refreshing/)
* [Facebook: more on tokens](https://developers.facebook.com/docs/facebook-login/access-tokens)
* [Python HOW: Making Facebook API calls Using facebook-sdk](https://drgabrielharris.medium.com/python-how-making-facebook-api-calls-using-facebook-sdk-ea18bec973c8)
* [TL;DR;full code - github for the above](https://github.com/DrGabrielHarris/Facebook-insight)

## Notes on errors

* This error, when posting a feed on the page, was a result of insufficient scope/permissions
  on the User Token
```
facebook.GraphAPIError: (#200) If posting to a group, requires app being installed in the group, and
          either publish_to_groups permission with user token, or both pages_read_engagement
          and pages_manage_posts permission with page token; If posting to a page,
          requires both pages_read_engagement and pages_manage_posts as an admin with
          sufficient administrative permission
```
So, the following permmissions/scope were selected,
  `pages_show_list, pages_read_engagement, pages_manage_posts, public_profile`,
  and the short-lived token was generated again,
  then the long-lived token was generated, then the page token that does not
  expire was generated.
