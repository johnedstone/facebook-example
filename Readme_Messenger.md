## Notes on messenger
* documentaion from Facebook and Python below good
* did add a developer for testing, under roles
* the following checkbox was not documented anywhere, but it's needed
in order to have the webhook used by messenger - screenshot: ![screenshot of fields to use](images/check_this.png)
* And, Messenger needs a "Business Verification" - screenshot: ![images/pages_messaging.png](images/pages_messaging.png)

## References
* [Facebook sample, with ngrox](https://developers.facebook.com/docs/messenger-platform/getting-started/sample-experience#local_install)
* [Facebook getting starte](https://developers.facebook.com/docs/messenger-platform/getting-started)
* [Facebook documentation Node.js](https://developers.facebook.com/docs/messenger-platform/getting-started/sample-experience#setup)
* [Python-Flask code](https://medium.com/nerd-for-tech/use-python-to-send-messages-to-facebook-messenger-api-837a18997ade)

<!--
# vim: ai et ts=4 sw=4 sts=4 nu
-->
