import logging
import os
import requests
import sys

from pprint import pprint

logging.basicConfig(
    level=logging.INFO, format="[%(asctime)s] %(message)s", datefmt="%d/%m/%y %H:%M:%S"
)

app_id = os.getenv('APP_ID')
app_secret = os.getenv('APP_SECRET')
user_short_token = os.getenv('USER_SHORT_TOKEN')

url = "https://graph.facebook.com/v11.0/oauth/access_token"

payload = {
    "grant_type": "fb_exchange_token",
    "client_id": app_id,
    "client_secret": app_secret,
    "fb_exchange_token": user_short_token,
}

# pprint(payload, indent=2)

try:
    response = requests.get(
        url,
        params=payload,
        timeout=5,
    )

except requests.exceptions.Timeout as e:
    logging.error("TimeoutError: {}".format(e))

else:
    try:
        response.raise_for_status()
    except Exception as e:
        logging.error('HTTPError: {}'.format(e))
        logging.error('response.json(): {}'.format(response.json()))

    else:
        response_json = response.json()
        logging.info(response_json)
        user_long_token = response_json["access_token"]

        #pprint(response.json(), indent=2)

"""
print(response_json)

{'access_token': 'EAAPxxxx', 'token_type': 'bearer', 'expires_in': 5183614}

"""
# vim: ai et ts=4 sw=4 sts=4 nu
