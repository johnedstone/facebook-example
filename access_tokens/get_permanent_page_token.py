import facebook
import os
from pprint import pprint

user_long_token = os.getenv('USER_LONG_TOKEN')
graph = facebook.GraphAPI(access_token=user_long_token, version="3.1")
pages_data = graph.get_object("/me/accounts")

#permanent_page_token = pages_data["data"][0]["access_token"]
#page_id = pages_data["data"][0]["id"]

pprint(pages_data)

"""
print(pages_data)

{'data': [{'access_token': 'EAAPxxx',
   'category': 'Education',
   'category_list': [{'id': '2250', 'name': 'Education'}],
   'name': 'Coding with Dr Harris',
   'id': '103361561317782',
   'tasks': ['ANALYZE', 'ADVERTISE', 'MODERATE', 'CREATE_CONTENT', 'MANAGE']}],
 'paging': {'cursors': {'before': 'MTAzMzYxNTYxMzE3Nzgy',
   'after': 'MTAzMzYxNTYxMzE3Nzgy'}}}
"""

# vim: ai et ts=4 sw=4 sts=4 nu
