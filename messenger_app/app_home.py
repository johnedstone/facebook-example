'''
See https://medium.com/nerd-for-tech/use-python-to-send-messages-to-facebook-messenger-api-837a18997ade
'''
from flask import Flask, request

app = Flask(__name__)

app.config['SECRET_KEY'] = 'enter-your-secret-key-here'

@app.route('/', methods=["GET", "POST"])
def home():
    return 'HOME'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8888', debug=True)

# vim: ai et ts=4 sts=4 sw=4 nu
