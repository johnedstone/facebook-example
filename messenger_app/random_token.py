#!/usr/bin/env python
'''
https://blog.miguelgrinberg.com/post/the-new-way-to-generate-secure-tokens-in-python
'''
import secrets

print(secrets.token_hex(20))
