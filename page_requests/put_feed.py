import logging
import os
import sys

from datetime import datetime
from pprint import pprint

import facebook

logging.basicConfig(
    level=logging.INFO, format="[%(asctime)s] %(message)s", datefmt="%d/%m/%y %H:%M:%S"
)

permanent_page_token = os.getenv('PERMANENT_PAGE_TOKEN')

graph = facebook.GraphAPI(access_token=permanent_page_token, version="3.1")

response = graph.put_object("me", "feed", message="Be the first to love - {}".format(datetime.now()))

logging.info(response)
sys.exit()

logging.info('-' * 40)
logging.info('This can also be done this way')

response = graph.put_object(
    parent_object="me",
    connection_name="feed",
    message="hello world {}".format(datetime.now()))

logging.info(response)
logging.info('-' * 40)

logging.info('{}'.format(
"""

In curl this would be:
(As worked out with the Graph API Explorer using the permanent Page token)

curl -i -X POST \
 "https://graph.facebook.com/v11.0/me/feed?message=goodby_world&access_token=<your token>"

"""))



# vim: ai et ts=4 sw=4 sts=4 nu
