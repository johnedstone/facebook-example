import facebook
import os
from pprint import pprint

permanent_page_token = os.getenv('PERMANENT_PAGE_TOKEN')
page_id = os.getenv('PAGE_ID')

graph = facebook.GraphAPI(access_token=permanent_page_token, version="3.1")
object = graph.get_object(id=page_id)
pprint(object, indent=4)

pprint('-' * 40)
pprint('This is the same as above')
object = graph.get_object("me")
pprint(object, indent=4)

pprint('-' * 40)

object = graph.get_object("me", metadata=1)
pprint(object, indent=4)

"""
From the Graph API Explorer
curl would be something like this

curl -i -X GET \
 "https://graph.facebook.com/v11.0/me?metadata=1&access_token=xxx"

and would return the metadata of the object based on the token
"""

pprint('-' * 40)

object = graph.get_object("me", metadata=1)
pprint(object, indent=4)

"""
From the Graph API Explorer
curl would be something like this

curl -i -X GET \
 "https://graph.facebook.com/v11.0/me?metadata=1&access_token=<your token>"

and would return the metadata of the object based on the token
"""
